import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class BackendServiceResource {
    private final BackendService backend;

    public BackendServiceResource(BackendService backend) {
        this.backend = backend;
    }

    private String renderResponse(BackendService.Status status) {
        switch (status) {
            case OK:
                return "Backend says that everything's all right!\n";
            case Slow:
                return "Backend seems to be a bit slow today.\n";
            case Unavailable:
                return "Backend does not respond properly... :(\n";
            default:
                return "I have no idea...\n";
        }
    }

    @GET
    @Path("/sync")
    @Produces(MediaType.TEXT_PLAIN)
    public String sync(@QueryParam("replyAfterMillis") long replyAfterMillis) throws InterruptedException {
        try {
            return backend.getStatus().map(this::renderResponse).toBlocking().first();
        } catch (RuntimeException e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @GET
    @Path("/async")
    @Produces(MediaType.TEXT_PLAIN)
    public void async(@QueryParam("replyAfterMillis") long replyAfterMillis,
                     @Suspended final AsyncResponse response) {
        backend.getStatus()
                .map(this::renderResponse)
                .subscribe(
                        response::resume,
                        (e) -> response.resume(new WebApplicationException(e)));
    }
}
