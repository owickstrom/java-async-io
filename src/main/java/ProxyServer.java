
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

public class ProxyServer {

    public static void main(String[] args) throws Exception {
        Server s = new Server(new QueuedThreadPool(10, 4));
        s.setConnectors(new Connector[]{createConnector(s)});
        ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/");
        ServletHolder servletHolder = new ServletHolder(createJerseyServlet());
        servletHolder.setInitOrder(1);
        context.addServlet(servletHolder, "/*");
        s.setHandler(context);
        s.start();
    }

    private static ServerConnector createConnector(Server s) {
        ServerConnector connector = new ServerConnector(s, 2, 2);
        connector.setHost("localhost");
        connector.setPort(8001);
        return connector;
    }

    private static ServletContainer createJerseyServlet() {
        ResourceConfig config = new ResourceConfig();
        BackendService backend = new LoggingBackendService(new LocalhostBackendService());
        config.register(new BackendServiceResource(backend));
        ServletContainer container = new ServletContainer(config);
        return container;
    }
}