import io.reactivex.netty.RxNetty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class LocalhostBackendService implements BackendService {
    private static final String HEALTH_URI = "http://localhost:3000?replyAfterMillis=2000";

    private Status statusFromHttpCode(int code) {
        if (code >= 200 && code < 300) {
            return Status.OK;
        } else {
            return Status.Unavailable;
        }
    }

    @Override
    public Observable<Status> getStatus() {
        return RxNetty.createHttpGet(HEALTH_URI)
                .map((response) -> response.getStatus().code())
                .map(this::statusFromHttpCode)
                .onErrorReturn((e) -> Status.Unavailable)
                .timeout(1, TimeUnit.SECONDS, Observable.just(Status.Slow));
    }
}
