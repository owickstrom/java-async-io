import rx.Observable;

public interface BackendService {
    Observable<Status> getStatus();

    public enum Status {
        OK, Slow, Unavailable;
    }
}
