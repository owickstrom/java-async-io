import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.util.concurrent.atomic.AtomicInteger;

public class LoggingBackendService implements BackendService {
    private static final Logger LOG = LoggerFactory.getLogger(LoggingBackendService.class);
    private static AtomicInteger pendingRequests = new AtomicInteger();
    private final BackendService delegate;

    public LoggingBackendService(BackendService delegate) {
        this.delegate = delegate;
    }

    private static void incrementCounter() {
        LOG.info("Pending requests: {}", pendingRequests.incrementAndGet());
    }

    private static void decrementCounter() {
        LOG.info("Pending requests: {}", pendingRequests.decrementAndGet());
    }

    private static void logStatus(Status status) {
        switch (status) {
            case Slow:
                LOG.warn("Backend request timed out.");
                return;
            case Unavailable:
                LOG.error("Backend did not respond.");
        }
    }

    @Override
    public Observable<Status> getStatus() {
        incrementCounter();
        return delegate.getStatus()
                .doOnNext(LoggingBackendService::logStatus)
                .finallyDo(LoggingBackendService::decrementCounter);
    }
}
