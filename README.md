# java-async-io

Just testing out async IO with Java 8, RxJava, JAX-RS and Netty.

## Build

If you have gradle installed.

```bash
gradle build
```

Otherwise...

```bash
./gradlew build
```
